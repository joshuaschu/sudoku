package SudokuKnacker;

public class Knacker {
    private int[][] sudoku;
    private int rows,columns;
    private boolean solved;
    public Knacker(int[][] sudoku) throws Exception{
        this.sudoku = sudoku;
        rows = this.sudoku.length;
        columns = this.sudoku[0].length;
        if (rows != columns && rows == 3 && columns == 3){
            throw new Exception("This format is not supported!");
        }
    }

    public void solve(){
        solved =false;
        this.solve(this.sudoku);
    }

    private void solve(int[][] toSolve){
        for (int row = 0;row<rows;row++){
            for(int column = 0;column<columns;column++){
                if (toSolve[row][column] == 0){
                   for (int num=1;num<10;num++){
                       if(solved)return;
                       toSolve[row][column]=num;
                       if (this.prove(toSolve,row,column,num)&&!solved){
                           this.solve(toSolve);
                       }
                       if (num==9&&!solved){
                           toSolve[row][column]=0;
                           return ;
                       }
                   }
                }
            }
        }
        solved=true;
    }

    private boolean prove(int[][] toSolve, int row, int column,int num) {
        for (int currRow=0; currRow<rows;currRow++){
            if (currRow == row){
                continue;
            }else if (toSolve[currRow][column] ==num) return false;
        }
        for (int currCol=0; currCol<rows;currCol++){
            if (currCol == column){
                continue;
            }else if (toSolve[row][currCol] ==num) return false;
        }
        for (int currRow = (row-(row%3));currRow<(row-(row%3))+3;currRow++){
            for (int currCol = (column-(column%3));currCol<(column-(column%3))+3;currCol++){
                if (currRow==row && currCol == column) continue;
                else if (toSolve[currRow][currCol]==num) return false;
            }
        }
        return true;
    }

    public int[][] getSudoku(){
        return this.sudoku;
    }
}
