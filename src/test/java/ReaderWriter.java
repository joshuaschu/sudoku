import SudokuKnacker.Knacker;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReaderWriter {
    private static WebDriver driver;
    private String pathToChromeDriver= "C:\\Users\\D070546\\Documents\\chromedriver_win32\\chromedriver.exe";
    private Knacker knacker;
    private int[][] soduko;

    @Test
    public void cheatOnSoduko() {
        System.setProperty("webdriver.chrome.driver", pathToChromeDriver);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.websudoku.com/?level=4");
        soduko = new int[9][9];
        WebElement rootWebElement = driver.findElement(By.tagName("frame"));
        driver.switchTo().frame(rootWebElement);

        for (int i = 0; i <10; i++) {
            WebElement element = driver.findElement(By.tagName("form"));
            WebElement child = element.findElement(By.id("puzzle_grid"));
            List<WebElement> values = child.findElements(By.tagName("input"));
            int counter = 0;
            for (WebElement e : values) {
                if (!e.getAttribute("value").equals("")) {
                    soduko[counter / 9][counter % 9] = Integer.parseInt(e.getAttribute("value"));
                } else {
                    soduko[counter / 9][counter % 9] = 0;
                }
                counter++;
            }
            System.out.println(Arrays.deepToString(soduko));
            try {
                knacker = new Knacker(soduko);
            } catch (Exception e) {
                e.printStackTrace();
            }
            knacker.solve();
            System.out.println(Arrays.deepToString(soduko));
            counter = 0;
            for (WebElement e : values) {
                e.sendKeys(soduko[counter / 9][counter % 9] + "");
                counter++;
                try {
                    //Thread.sleep(750);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            WebElement okButton = element.findElement(By.name("submit"));
            okButton.click();
            try {
                Thread.sleep(2000);
            } catch (Exception ex1) {
                ex1.printStackTrace();
            }

            element = driver.findElement(By.tagName("form"));
            WebElement statsButton = element.findElement(By.name("showstats"));
            statsButton.click();
            try {
                Thread.sleep(2000);
            } catch (Exception ex1) {
                ex1.printStackTrace();
            }
            element = driver.findElement(By.tagName("form"));
            WebElement newButton = element.findElement(By.name("newgame"));
            newButton.click();
        }
    }
}
