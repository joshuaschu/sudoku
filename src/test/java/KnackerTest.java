import SudokuKnacker.Knacker;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class KnackerTest {
    @Test
    public void test(){
        int[][] testSudoku = new int[9][9];
        testSudoku[0][0] = 0;
        testSudoku[0][1] = 0;
        testSudoku[0][2] = 0;
        testSudoku[0][3] = 5;
        testSudoku[0][4] = 0;
        testSudoku[0][5] = 0;
        testSudoku[0][6] = 0;
        testSudoku[0][7] = 0;
        testSudoku[0][8] = 7;

        testSudoku[1][0] = 6;
        testSudoku[1][1] = 0;
        testSudoku[1][2] = 7;
        testSudoku[1][3] = 0;
        testSudoku[1][4] = 0;
        testSudoku[1][5] = 0;
        testSudoku[1][6] = 0;
        testSudoku[1][7] = 4;
        testSudoku[1][8] = 0;

        testSudoku[2][0] = 0;
        testSudoku[2][1] = 0;
        testSudoku[2][2] = 0;
        testSudoku[2][3] = 3;
        testSudoku[2][4] = 0;
        testSudoku[2][5] = 6;
        testSudoku[2][6] = 0;
        testSudoku[2][7] = 0;
        testSudoku[2][8] = 0;

        testSudoku[3][0] = 0;
        testSudoku[3][1] = 5;
        testSudoku[3][2] = 0;
        testSudoku[3][3] = 0;
        testSudoku[3][4] = 3;
        testSudoku[3][5] = 8;
        testSudoku[3][6] = 6;
        testSudoku[3][7] = 2;
        testSudoku[3][8] = 0;

        testSudoku[4][0] = 2;
        testSudoku[4][1] = 0;
        testSudoku[4][2] = 0;
        testSudoku[4][3] = 4;
        testSudoku[4][4] = 0;
        testSudoku[4][5] = 1;
        testSudoku[4][6] = 0;
        testSudoku[4][7] = 0;
        testSudoku[4][8] = 9;

        testSudoku[5][0] = 0;
        testSudoku[5][1] = 1;
        testSudoku[5][2] = 6;
        testSudoku[5][3] = 2;
        testSudoku[5][4] = 5;
        testSudoku[5][5] = 0;
        testSudoku[5][6] = 0;
        testSudoku[5][7] = 7;
        testSudoku[5][8] = 0;

        testSudoku[6][0] = 0;
        testSudoku[6][1] = 0;
        testSudoku[6][2] = 0;
        testSudoku[6][3] = 6;
        testSudoku[6][4] = 0;
        testSudoku[6][5] = 5;
        testSudoku[6][6] = 0;
        testSudoku[6][7] = 0;
        testSudoku[6][8] = 0;

        testSudoku[7][0] = 0;
        testSudoku[7][1] = 6;
        testSudoku[7][2] = 0;
        testSudoku[7][3] = 0;
        testSudoku[7][4] = 0;
        testSudoku[7][5] = 0;
        testSudoku[7][6] = 9;
        testSudoku[7][7] = 0;
        testSudoku[7][8] = 8;

        testSudoku[8][0] = 9;
        testSudoku[8][1] = 0;
        testSudoku[8][2] = 0;
        testSudoku[8][3] = 0;
        testSudoku[8][4] = 0;
        testSudoku[8][5] = 7;
        testSudoku[8][6] = 0;
        testSudoku[8][7] = 0;
        testSudoku[8][8] = 0;

        try {
            Knacker k= new Knacker(testSudoku);
            k.solve();
            System.out.println(Arrays.deepToString(k.getSudoku()));
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
